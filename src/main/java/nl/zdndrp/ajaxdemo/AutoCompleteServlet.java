/*
 * © 2017 Fred Zuijdendorp
 */
package nl.zdndrp.ajaxdemo;

import java.io.IOException;
import java.util.HashMap;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author fred
 */
@WebServlet(name = "AutoCompleteServlet", urlPatterns = {"/autocomplete"})
public class AutoCompleteServlet extends HttpServlet {

	private ServletContext context;
	private final ComposerData compData = new ComposerData();
	private final HashMap<String, Composer> composers = compData.getComposers();

	@Override
	public void init(ServletConfig config) throws ServletException {
		this.context = config.getServletContext();
	}

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String path = request.getPathInfo();
		String queryString = request.getQueryString();
		System.out.printf("--> GET: %s%s%n", path, queryString != null ? "?" + queryString : "");
		String action = request.getParameter("action");
		String search = request.getParameter("search");
		String targetId = request.getParameter("id");
		StringBuilder sb = new StringBuilder();

		boolean namesAdded = false;
		switch (action) {
			case "complete":
				// check if user sent empty string
				if (search != null && (search = search.trim()).length() > 0) {
					search = search.toLowerCase();
					for (String id : composers.keySet()) {
						Composer composer = (Composer) composers.get(id);
						if ( // targetId matches first name
								composer.getFirstName().toLowerCase().startsWith(search)
								|| // targetId matches last name
								composer.getLastName().toLowerCase().startsWith(search)
						) {
							sb.append("<composer>");
							sb.append("<id>").append(composer.getId()).append("</id>");
							sb.append("<firstName>").append(composer.getFirstName()).append("</firstName>");
							sb.append("<lastName>").append(composer.getLastName()).append("</lastName>");
							sb.append("</composer>");
							namesAdded = true;
						}
					}
				}
				else {
					context.getRequestDispatcher("/error.jsp").forward(request, response);
					break;
				}

				if (namesAdded) {
					response.setContentType("text/xml");
					response.setHeader("Cache-Control", "no-cache");
					response.getWriter().write("<composers>" + sb.toString() + "</composers>");
				}
				else {
					//nothing to show
					response.setStatus(HttpServletResponse.SC_NO_CONTENT);
				}
				break;
			case "lookup":
				// put the target composer in the request scope to display 
				if ((targetId != null) && composers.containsKey(targetId.trim())) {
					request.setAttribute("composer", composers.get(targetId));
					context.getRequestDispatcher("/composer.jsp").forward(request, response);
				}
				else {
					//nothing to show
					response.setStatus(HttpServletResponse.SC_NO_CONTENT);
				}
				break;
			case "jquery":
				if (search != null) {
					search = search.trim();
					request.setAttribute("search", search);
					context.getRequestDispatcher("/jquery-ajax.jsp").forward(request, response);
				}
				else {
					context.getRequestDispatcher("/jquery.jsp").forward(request, response);
				}
				break;
		}
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 *
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

}
