<%-- 
    Document   : error
    Created on : Jan 14, 2017, 4:52:29 PM
    Author     : fred
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="stylesheet.css">      
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search Error</title>
		<link href="css/stylesheet.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <h2>Search Error</h2>

        <p>An error occurred while performing the search. Please try again.</p>

		<p><a href="autocomplete.html" class="link">back</a></p>
    </body>
</html>
