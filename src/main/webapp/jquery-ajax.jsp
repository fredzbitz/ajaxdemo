<%-- 
    Document   : jquery-ajax
    Created on : Jan 15, 2017, 5:34:21 PM
    Author     : fred
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table class="w3-table-all w3-margin-top" id="myTable">
	<tr>
		<th style="width:40%;">firstname</th>
		<th style="width:40%;">lastname</th>
		<th style="width:20%;">category</th>
	</tr>
	<c:forEach var="composer" items="${composerData.getComposerList(requestScope.search)}">
		<tr>
			<td>${composer.firstName}</td>
			<td>${composer.lastName}</td>
			<td>${composer.category}</td>
		</tr>
	</c:forEach>
</table>
