<%-- 
    Document   : jquery
    Created on : Jan 15, 2017, 4:08:00 PM
    Author     : fred
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JQuery Demo</title>
		<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
		<script src="http://code.jquery.com/jquery-latest.js"></script>
		<script>
			$(document).ready(function () {
				$("#search").keyup(function (event) {
					var search = $("#search").val();
					$.get("autocomplete", {action: "jquery", search: search}, function (responseText) {
						$("#complete-table").html(responseText);
					});
				});
			});
		</script>
	</head>
    <body>
		<div class="w3-container">
			<h1>Hello JQuery!</h1>
			<input class="w3-input w3-border w3-padding" type="text" placeholder="Search for names.." id="search">
			<div id="complete-table">
				<table class="w3-table-all w3-margin-top" id="myTable">
					<tr>
						<th style="width:40%;">firstname</th>
						<th style="width:40%;">lastname</th>
						<th style="width:20%;">category</th>
					</tr>
					<c:forEach var="composer" items="${composerData.composerList}">
						<tr>
							<td>${composer.firstName}</td>
							<td>${composer.lastName}</td>
							<td>${composer.category}</td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>
    </body>
</html>
