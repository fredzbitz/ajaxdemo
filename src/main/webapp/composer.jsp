<%-- 
    Document   : composers
    Created on : Jan 14, 2017, 4:51:42 PM
    Author     : fred
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<title>Composer Information</title>
		<link rel="stylesheet" type="text/css" href="stylesheet.css">
		<link href="css/stylesheet.css" rel="stylesheet" type="text/css"/>
	</head>
	<body>

		<table>
			<tr>
				<th colspan="2">Composer Information</th>
			</tr>
			<tr>
				<td>First Name: </td>
				<td>${requestScope.composer.firstName}</td>
			</tr>
			<tr>
				<td>Last Name: </td>
				<td>${requestScope.composer.lastName}</td>
			</tr>
			<tr>
				<td>ID: </td>
				<td>${requestScope.composer.id}</td>
			</tr>
			<tr>
				<td>Category: </td>
				<td>${requestScope.composer.category}</td>
			</tr>      
		</table>

		<p><a href="autocomplete.html" class="link">back</a></p>
	</body>
</html>
