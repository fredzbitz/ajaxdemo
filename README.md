# README #

Ajax demo.

### What is this repository for? ###

* demo 1: filtering html without Ajax
* demo 2: filtering with JavaScript, HTML, servlet
* demo 3: filtering with JQuery, JSP

### How do I get set up? ###

* open project in NetBeans
* run

### Who do I talk to? ###

* Fred Zuijdendorp
* fred.zuijdendorp@mac.com